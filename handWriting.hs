import Graphics.Gloss
import Graphics.Gloss.Data.Point
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Interface.Pure.Game
import Graphics.Rendering.Cairo

import Graphics.Gloss.Interface.IO.Game

import Path
import Path.IO
import System.Environment
import System.IO
import Control.Monad

import Codec.Picture
import Codec.Picture.Png
import Codec.Picture.Types
import qualified Data.ByteString.Lazy as B
import Control.Monad.IO.Class
import Control.Monad

-- File name to store the state
fileName :: FilePath
fileName = "debug.txt"

-- Function to read the state from file
readState :: MyWorld
readState = do
  contents <- readFile fileName
  return (map (\[x, y] -> (x, y)) (map (map read . words) (lines contents)))

-- Function to write the state to file
writeState :: [Point] -> IO ()
writeState state = do
  writeFile fileName (unlines (map (unwords . map show . (\(x, y) -> [x, y])) state))

-- Initial state of the application
initialState :: MyWorld
initialState = do
  state <- readState
  return $ if null state then [] else state

window :: Display
window = InWindow "Handwriting" (800, 600) (10, 10)

type MyWorld = IO [Point]

background :: Color
background = black --white

drawing :: [Point] -> IO Picture
drawing points =
  return $ Color white $ pictures $ map line $ segmentList points

segmentList :: [Point] -> [[Point]]
segmentList points =
  case points of
    [] -> []
    [_] -> []
    (p1:p2:ps) -> [p1,p2] : segmentList (p2:ps)

convertIO :: IO () -> IO [()]
convertIO action = do
  result <- action
  return [result]

myConvertIO :: IO () -> IO [()]
myConvertIO = liftM (\x -> [x])

handleEvents :: Event -> [Point] -> MyWorld
handleEvents (EventKey (MouseButton LeftButton) Down _ point) points = do
--  putStr "Point:   "
--  print point
  print "rgv"
  putStr $ "Points: " <>  show points
  return $ point : points
handleEvents (EventKey (Char 's') Down _ _) points = do
  writeState points
--  liftIO $  writeState points
  return $ points
  {-
let pic = drawing points
  points
  liftIO $ B.writeFile "canvas.png" (encodePng img)
  -}
--  savePicture "canvas.png" (800,600) (drawing points) >> return points
handleEvents _ points = return $ points

{-
scalePic :: Picture -> Image PixelRGBA8
scalePic pic =
  let (w,h) = (800, 600)
      img = Image (0,0) (w,h) pic
  in img
renderPic :: Picture -> Image PixelRGBA8
renderPic pic =
  let (w,h) = (800, 600)
      img = generateImage (\x y -> let c = pixelAt pic x y
                                  in PixelRGBA8 c c c 255) w h
  in img
-}

update :: Float -> [Point] -> MyWorld
update _ points = return $ points

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> error "no arguments!!!"
    [x] -> do
      state <- initialState
      playIO window background 60 state drawing  handleEvents update
    _ -> error "multiple arguments"

--    https://github.com/soupi/haskell-study-plan