sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples factors limit = res1 - res2
  where res1 = sum $ map (\x -> helper x limit) resFactors
        res2 = if length (resFactors) == 0 || length ( resFactors ) == 1 then  0 else product resFactors
        resFactors = filter (<limit) factors

helper x limit = x * (k*(k+1)`div` 2)
     where k = kFinder x
           kFinder x = (limit - 1) `div` x

exec limit xs  = [m | m <- [1..newLimit] , remBy xs m]
                            where newLimit = limit - 1
                             
remBy xs ele = foldr helper False xs
  where helper x True = True
        helper x _ = ele `rem` x == 0

result limit xs = sum $ exec limit xs
