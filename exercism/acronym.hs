
import Data.List
import Data.Char

abbreviate :: String -> String
abbreviate = map head . words

splitAtCond :: (a -> Bool) -> [a] -> ([a],[a])  --(Eq a) =
splitAtCond p xs = (takeWhile (not . p) xs, dropWhile (not . p) xs)

test = "HyperText Markup Language"

firstSplit xs = (y:z1,z2) 
  where (y,ys) = (head xs,tail xs)
        (z1,z2) = splitAtCond isUpper ys
