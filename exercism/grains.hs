-- module Grains (square, total) where

square :: Integer -> Maybe Integer
square n
  | n <= 0 = Nothing
  | otherwise = Just $ 2^(n-1) --error "You need to implement this function."

total :: Integer
total = foldr (\x acc -> 2^x+acc) 0 [0..63]
