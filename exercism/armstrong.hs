digits :: Int -> [Int]
digits num = go num []

go 0 acc = acc
go num acc = go rest (digit : acc)   --until (==0) (
  where (rest,digit) = divMod num 10

armStrong num = sumArm == num 
  where n = length res
        res = digits num
        sumArm = sum $ map (^n) res
