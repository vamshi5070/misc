decToBin :: Integer -> [Integer]
decToBin 0 = [0]
decToBin n = reverse $ go n
  where
    go 0 = []
    go x = let (q,r) = x `divMod` 2
           in r : go q
