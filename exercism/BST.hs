
data BST a = Empty | Node a (BST a) (BST a)
  deriving (Eq,Show)

bstLeft :: BST a -> Maybe (BST a)
bstLeft Empty =  Nothing
bstLeft (Node _ Empty _) =  Nothing
bstLeft (Node _ l _) =  Just l

bstRight :: BST a -> Maybe (BST a)
bstRight Empty = Nothing
bstRight (Node _ _ Empty) = Nothing
bstRight (Node _ _ r) =  Just r

bstValue :: BST a -> Maybe a
bstValue Empty = Nothing
bstValue (Node x _ _) = Just x
-- bstValue (Node x l r) =  Just r

--tree = Node 1 (Node 2 Empty Empty) (Node 3 Empty Empty)

empty :: BST a
empty = Empty

-- fromList :: Ord a => [a] -> BST a
-- fromList xs = 

-- insert :: Ord a => a -> BST a -> BST a
-- insert x Empty = Node x Empty Empty
-- insert x (Node y l) = Node x Empty Empty


insert :: (Ord a) => a -> BST a -> BST a
insert key Empty = singleton key
insert key (Node x l r)
  | key < x = Node x (insert key l) r
  | key > x = Node x l (insert key r)
  | otherwise = Node x l r

fromList :: (Ord a) => [a] -> BST a --create BST out of a list of elements
fromList xs = foldr insert Empty xs

singleton :: a -> BST a
singleton x = Node x Empty Empty

-- toList :: BST a -> [a]
-- toList tree =
{-
preOrder :: (Ord a) => BST a -> [a]
preOrder Empty = []
preOrder (Node x Empty Empty)  = [x]
preOrder (Node x l r) = [x] <> (preOrder l) <> (preOrder r)

 
inOrder :: (Ord a) => BST a -> [a]
inOrder Empty = []
inOrder (Node x Empty Empty)  = [x]
inOrder (Node x l r) = (inOrder l) <> [x]  <> (inOrder r)
-}

toList :: (Ord a) => BST a -> [a]
toList Empty = []
toList (Node x Empty Empty)  = [x]
toList (Node x l r) = (toList l) <> [x]  <> (toList r)

