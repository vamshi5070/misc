canAttack :: (Int, Int) -> (Int, Int) -> Bool
canAttack (r1,c1) (r2,c2)
  | r1 == r2 || c1 == c2 = True
  | abs (r1-r2) == abs (c1-c2) = True
  | otherwise = False

-- boardString :: Maybe (Int, Int) -> Maybe (Int, Int) -> String
-- boardString 

setColumn num xs = foldr helper (7,[]) xs
  where helper x (n,acc) 
          | n == num = 
