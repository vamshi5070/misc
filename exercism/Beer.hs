line :: Int -> String
line x = line1 x <> "\n" <> line2 x

line1 :: Int -> String
line1 x = (show x) <> " bottles of beer on the wall, " <> (show x) <>  " bottles of beer." 

line2 :: Int -> String
line2 x = "Take one down and pass it around, " <> (show (x-1)) <> " bottles of beer on the wall."

upto3 :: String ->  String
upto3 prev = foldr (\x acc -> line x <> "\n\n" <> acc ) prev [99,98..3]-- line 99 <> "\n\n" <> line 98

full :: String
full = upto3 nonAutomatable

nonAutomatable = "2 bottles of beer on the wall, 2 bottles of beer.\n\
\Take one down and pass it around, 1 bottle of beer on the wall.\n\
\\n\
\1 bottle of beer on the wall, 1 bottle of beer.\n\
\Take it down and pass it around, no more bottles of beer on the wall.\n\
\\n\
\No more bottles of beer on the wall, no more bottles of beer.\n\
\Go to the store and buy some more, 99 bottles of beer on the wall.\n"
