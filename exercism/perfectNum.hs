data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)


classify :: Int -> Maybe Classification
classify n
	| n <= 0 = Nothing
	| n == sumFac = Just Perfect
	| n < sumFac = Just Abundant
	| otherwise = Just Deficient 
  where sumFac = sum . factors $ n

factors :: Int -> [Int]
factors num = [ x|  x <- [1..(num `div` 2)] , num `mod` x == 0]
