
keepDiscard :: (a -> Bool) -> [a] -> ([a],[a])
keepDiscard p xs = foldr (\x (ks,ds) -> if p x then (x:ks,ds) else (ks,x:ds)) ([],[]) xs

discard :: (a -> Bool) -> [a] -> [a]
discard p xs = snd $ keepDiscard p xs
--error "You need to implement this function."

keep :: (a -> Bool) -> [a] -> [a]
keep p xs = fst $ keepDiscard p xs


