import Data.Time.LocalTime
import Data.Time.Format
import Control.Monad
import Control.Monad.Trans

main = do
  date <- liftIO $ fmap (formatTime defaultTimeLocale "%Y-%m-%d") getZonedTime
  time <- liftIO $ fmap (formatTime defaultTimeLocale "%H-%M-%S") getZonedTime
  
  print $ time
