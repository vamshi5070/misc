import System.Clock

main = do
  timer 0 0 0 

timer min sec milySec
  | milySec == 1000 = do
      putStrLn $ show (min) <> ":" <> show (sec+1) <> ":" <> show (0)
      timer min (sec+1) 0 
  | sec > 59 = do
      putStrLn $  show (min+1) <> ":" <> show (0) <> ":" <> show (milySec+1)  
      timer (min+1) 0 (milySec+1)
  | otherwise = do
      putStrLn $ show (min) <> ":" <> show (sec) <> ":" <> show (milySec+1)
      timer (min) (sec) (milySec+1)
