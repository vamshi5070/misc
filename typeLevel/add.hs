{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances, UndecidableInstances
#-}

import Prelude hiding (succ)

data Z
data S n

class Succ i1 i | i1 -> i

instance Succ Z (S Z)

instance Succ (S x) (S (S x))

succ :: Succ i1 i => i1 -> i
succ = const undefined

class Add i1 i2 i | i1 i2 -> i , i1 i -> i2

instance Add Z i2 i2

instance (Add i1 i2 i) => Add (S i1) i2 (S i)

add :: Add i1 i2 i => i1 -> i2 -> i
add = const undefined
