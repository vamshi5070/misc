import Data.Array

vamshi =1

inserts :: ( Ord a ) => a -> [a] -> [a]
inserts x [] = [x]
inserts x (y:ys)
  | x <= y = x:y:ys
  | otherwise = y : inserts x ys
