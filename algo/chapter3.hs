-- Data structure
type SymList a = ([a],[a])

data RGV = Knuth

fromSL :: SymList a -> [a]
fromSL (xs,ys) = xs ++ reverse ys

headSL :: SymList a -> a
-- headSL ([],ys) = head ys
headSL (xs,ys)
  | not (null xs)   = head xs
  | single ys = head xs
  | otherwise = error "fucked up"

single [x] = True
single _ = False

toSL :: [a] -> SymList a
toSL [] = ([],[])
toSL [x] = ([x],[])
toSL xs = (us,reverse vs)
  where (us,vs) = splitAt (length xs `div` 2) xs
-- toSL [x]

inits :: [a] -> [[a]]
inits [] = [[]]
inits (x:xs) = []: map (x:) (inits xs)

-- test = ([])

ex = [1,2,3,4,5]
ex1 :: (Eq a) => [a]
ex1 = []
ex2 = [1]
ex3 = [1,2]

{-
null xs = null ys || single ys
null ys = null xs || single xs
  -}

test :: Bool
test = lhs == rhs
  where rhs = headSL . toSL $ (ex3:: [Int])
        lhs = head ex3

lastSL :: SymList a -> a
lastSL (xs,ys)
  | not (null ys) = head ys
  | null ys = last xs   -- last or head , it doesn't matter
  -- | null xs = head ys
  | otherwise = error "empty list"

snocSL :: a -> SymList a -> SymList a
