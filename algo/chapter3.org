* Symmetric lists
#+begin_src haskell :tangle dataStruct.hs
type SymList  a = ([a],[a])
#+end_src

* Natural transformation 
The list is split into two equal halves (mostly) and then the second list is reversed 
#+begin_src haskell :tangle dataStruct.hs
fromSL :: SymList a -> [a]
fromSL (xs,ys) = xs ++ (reverse ys)
#+end_src
* Example:-
#+begin_src haskell :results code
:l dataStruct.hs
fromSL ([1,2],[3,4])
#+end_src
#+RESULTS:
#+begin_src haskell
[1,2,4,3]
#+end_src
* Main
#+begin_src haskell :tangle dataStruct.hs
-- main is about main
main = return ()
#+end_src

