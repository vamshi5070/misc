main = return ()

inserts :: a -> [a] -> [[a]]
inserts x [] = [[x]]
inserts x (y:ys) = (x:y:ys) : map (y:) (inserts x ys)

collapse :: [[Int]] -> [Int]
--collapse = foldl helper []
--	where helper  xs acc
--		| sum acc > 0 = acc
--		| otherwise =  acc <> xs

collapse = helper []
  where helper acc [] = acc
        helper acc (xs:xss)
          | sum acc > 0  = acc
          | otherwise = helper (acc <> xs) xss

-- collapse = help (
labelSum :: (Foldable t, Num a) => [t a] -> [(a, t a)]
labelSum xss = zip (map sum xss) xss

-- help (_,acc) [] = acc
-- help (s,acc) (xs:xss)
--   | s > 0 = acc
--   | otherwise = help ((s + s'),(acc <> xs')) xss
--   where (s',xs') = xs

help (s,xs) xss = if s > 0 || null xss then xs
  else help (cat (s,xs) (head xss)) (tail xss)

cat (x,xs) (y,ys) = (x+y, xs<> ys)

collapse'  xss = help (0,[]) (labelSum xss)
