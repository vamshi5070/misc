

-- chapter 4 . Binary Search

import Library


-- linear search
--            f           target  list of indices
search :: (Nat -> Nat) -> Nat -> [Nat]
search f t = [y | y <- [0..t], t == f y ]

-- binary search
search' :: (Nat -> Nat) -> Nat -> [Nat]
search' f t = seek (0,t)
    where seek (x,y)
            | x > y = []
            | t < f m = seek (x,m-1)
            | t == f m = [m]
            | t > f m = seek (m+1,y)
            | otherwise = [z | z <- [x..y], t == f z]
            where m = choose (x, y)

bound :: (Nat -> Nat) -> Nat -> (Int,Nat)
bound f t
    | t <= f 0 = (-1,0)
    | otherwise = (b `div` 2, b)
    where b = until done (*2) 1
          done b = t <= f b

search'' f t  = if f x == t then [x] else []
    where x= smallest (bound f t) f t

smallest (a, b) f t
    | a + 1 == b = b
    | t <= f m = smallest (a,m) f t
    | otherwise = smallest (m,b) f t
    where m = (a + b) `div` 2