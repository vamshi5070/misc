-- |

main = interact $ unlines .   map (show . uncurryList commonDivisors . map (read :: String -> Integer) . words) .  tail   . lines

uncurryList :: (a -> a -> b) -> [a] -> b
uncurryList f [x,y] = f x y
uncurryList f _ = error "mismatched length"

commonDivisors :: (Integral a) => a -> a -> Int
commonDivisors m l
  | m == l = 2
  | m == 1 || l == 1 = 2
  | otherwise = actualRes
  where
        limit = if minNum /= 1 then minNum  `div` 2 else 1
        minNum = min m l
        predicate num = m `mod` num  == 0 && l `mod` num == 0
        actualRes = length resultList
        resultList = [x | x <- [1..limit], predicate x]



