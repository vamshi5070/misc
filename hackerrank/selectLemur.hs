count n k
  | k == 0 = 1
  | k == n = 1
  | otherwise = count (n-1) (k-1) + count (n-1) k 

