data BTree a = Empty | Node a (BTree a) (BTree a)
  deriving (Eq,Show)

insertBTree :: (Ord a) => a -> BTree a -> BTree a
insertBTree key Empty = Node key Empty Empty
insertBTree key (Node x l r)
  | key < x = Node x (insertBTree key l) r
  | key > x = Node x l (insertBTree key r)
  | otherwise = Node x l r
  
createBTreeR :: (Ord a) => [a] -> BTree a --create BTree out of a list of elements
createBTreeR xs = foldr insertBTree Empty xs

depthTree :: (Eq a) => BTree a  -> a -> Maybe Int
depthTree Empty _ = Just 0
depthTree (Node x Empty Empty) key 
  | x == key = Just 1
  | otherwise = Nothing
depthTree (Node x Empty r) key
  | x == key = Just 1
  | otherwise = (+1) <$> depthTree r key
depthTree (Node x l Empty) key
  | x == key = Just 1
  | otherwise = (+1) <$> depthTree l key
depthTree (Node x l r) key
  | x == key = Just 1
  | otherwise = (+1) <$> case depthTree l key of
                           Nothing -> depthTree r key
                           Just d -> Just d
      

tree = Node 1 (Node 2 Empty Empty) (Node 3 (Node 4 Empty Empty) Empty)

prettyPrintTree :: Show a => BTree a -> IO ()
prettyPrintTree tree = putStr (unlines (prettyPrintIndented tree))

prettyPrintIndented :: Show a => BTree a -> [String]
prettyPrintIndented Empty = ["-"]
prettyPrintIndented (Node value left right) = 
    let valueStr = show value
        leftIndented = prettyPrintIndented left
        rightIndented = prettyPrintIndented right
        indentStr = replicate (length valueStr + 2) ' '
    in (valueStr : map (\s -> indentStr ++ s) leftIndented) ++
       map (\s -> indentStr ++ s) rightIndented

