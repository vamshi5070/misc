import Data.List

commonDivisors :: Int -> Int -> [Int]
commonDivisors x y = intersect (divisors x)  (divisors y)
--

main = interact $ unlines .   map (show . uncurryList lenCommonDiv . map (read :: String -> Integer) . words) .  tail   . lines

divisors :: Int -> [Int]
divisors 1 = [1]
divisors n = [x | x <- [1..n],  n `mod` x ==0 ]

lenCommonDiv x y = length $ commonDivisors x y

uncurryList :: (a -> a -> b) -> [a] -> b
uncurryList f [x,y] = f x y
uncurryList f _ = error "mismatched length"

