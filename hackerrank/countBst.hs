factorial n = product [1..n]

catalanNum n = factorial (2*n) `div` (factorial (n+1) * factorial n)

shortAns x =   x `mod` (100000007)

main = interact $ unlines .  map (show . shortAns . catalanNum . (read ::  String -> Integer)) .  tail . lines
