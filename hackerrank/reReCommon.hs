-- Enter your code here. Read input from STDIN. Print output to STDOUT
import Data.List

-- commonDivisors :: Integral a => a -> a -> [a]
-- commonDivisors num1 num2 =  [x | x <- [1..num1], x <- [1..num2] ,  n `mod` x ==0 ]

  -- intersect (divisors x)  (divisors y)

main = interact $ unlines .   map (show . uncurryList lenCommonDiv . map (read :: String -> Integer) . words) .  tail   . lines

-- divisors :: (Integral a) => a -> [a]
-- divisors 1 = [1]
-- divisors n = [x | x <- [1..n],  n `mod` x ==0 ]

lenCommonDiv x y = length $ commonDivisors x y

uncurryList :: (a -> a -> b) -> [a] -> b
uncurryList f [x,y] = f x y
uncurryList f _ = error "mismatched length"

commonDivisors :: Integral a => a -> a -> [a]
commonDivisors num1 num2 = foldr (\x acc -> if num1 `mod` x == 0 && num2 `mod` x == 0 then x:acc else acc  ) [] [1..minNum]
  where minNum = floor $ sqrt $ fromIntegral $ min num1 num2

findCommonDivisors :: Integral a => a -> a -> [a]
findCommonDivisors a b = divisors (gcd a b)
  where
    euclid x y = if y == 0 then x else euclid y (x `mod` y)
    divisors n = foldr (\x acc -> if n `mod` x == 0
                                   then if x == n `div` x
                                          then x : acc
                                          else x : n `div` x : acc
                                   else acc) [] [1..sqrtInt n]
    sqrtInt = floor . sqrt . fromIntegral

  -- Int -> Int -> [Int]
