data BST a = Empty | Node a (BST a) (BST a)
  deriving (Eq,Show)

insertBST :: (Ord a) => a -> BST a -> BST a
insertBST key Empty = Node key Empty Empty
insertBST key (Node x l r)
  | key < x = Node x (insertBST key l) r
  | key > x = Node x l (insertBST key r)
  | otherwise = Node x l r
  
createBSTR :: (Ord a) => [a] -> BST a --create BST out of a list of elements
createBSTR xs = foldr insertBST Empty xs

createBSTL :: (Ord a) => [a] -> BST a --create BST out of a list of elements
createBSTL xs = foldl (flip insertBST) Empty xs

preOrder :: (Ord a) => BST a -> [a]
preOrder Empty = []
preOrder (Node x Empty Empty)  = [x]
preOrder (Node x l r) = [x] <> (preOrder l) <> (preOrder r)

return2nd :: [a] -> [a]
return2nd = fst . foldr helper ([],True)
  where helper x (acc,flag)
          | flag = (x:acc,False)
          | otherwise = (acc,True)
          
return2ndWithFunc :: (a -> b) -> [a] -> [b]
return2ndWithFunc f = fst . foldr helper ([],True)
  where helper x (acc,flag)
          | flag = (f x:acc,False)
          | otherwise = (acc,True)

result :: (Ord a) => [a] -> String
result xs 
  | xs == (preOrder  $ createBSTL xs) = "YES"
  | otherwise = "NO"

main = interact $  unlines . return2ndWithFunc (result . (map (read :: String -> Int)) . words) .  tail . lines
