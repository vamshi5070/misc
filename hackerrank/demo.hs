min' x y
  | x <= y = x
  | otherwise = y

minimum' [] = error "empty list"
minimum' (x:xs) = x `min'` minimum' xs

main = return ()

-- max :: a ->
max' :: (Ord a) => a -> a -> a
max' x y
  | x >= y = x
  | otherwise = y
