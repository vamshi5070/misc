--import 

main = undefined

rows :: Int -- no of rows in grid
rows = 6

cols :: Int  -- no of cols in grid
cols  = 7

win :: Int -- no of consecutive blocks to win
win = 4

depth :: Int -- length of tree
depth = 4

type Board = [Row]
type Row = [Block]


data Block =   O | B | X
	deriving (Eq,Show,Ord)
