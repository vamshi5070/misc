type Grid = Matrix Value

type Matrix a = [Row a]

type Row a = [a]

type Value = Char

type Choices = [Value]

blank :: Grid
blank = rep9 (rep9 '.')
  where
    rep9 = replicate 9

rows :: Matrix a -> [Row a]
rows = id

cols = transpose

-- transpose :: Matrix a -> Matrix a
transpose :: [[a]] -> [[a]]
transpose [] = []
transpose ([] : xss) = transpose xss
transpose xss = (map head xss) : (transpose (map tail xss))

boxs :: Matrix a -> [Row a]
boxs [] = []
boxs xs = concat $ map (map concat) $ map transpose $ map (map chop) $ chop xs

chop :: [a] -> [[a]]
chop [] = []
chop xs = take 3 xs : (chop (drop 3 xs))

--matrices = []
valid g =
  cond (rows g)
    && cond (cols g)
    && cond (boxs g)
  where
    cond = all $ not . hasDup

hasDup :: (Eq a) => [a] -> Bool
hasDup xs = snd $ foldr helper ([], False) xs
  where
    helper _ (_, True) = ([], True)
    helper x (acc, pred)
      | x `elem` acc = (acc, True)
      | otherwise = (x : acc, pred)

choices :: Grid -> Matrix Choices
choices g = map (map choice) g
  where
    choice '.' = ['1' .. '9']
    choice ch = [ch]

-- collapse :: Matrix [a] -> [Matrix a]
-- collapse gs =

mine xs ys zs ks = do
  x <- xs
  y <- ys
  z <- zs
  k <- ks
  return $ [x, y, z, k]

mine1 xs ys zs ks = [[x, y, z, k] | x <- xs, y <- ys, z <- zs, k <- ks]

-- cp xs [] = [xs]
-- cp [] xs = [xs]
-- cp xs ys = do
--   x <- xs
--   y <- ys
--   return $ [x,y]

-- coll [] = []
-- coll xss = foldr helper [[]]  xss
--   where helper x acc = concatMap (cp x) acc

cp :: [[a]] -> [[a]]
cp [] = [[]]
cp (xs : xss) = [x : zs | x <- xs, zs <- cp xss]

collapse :: Matrix [a] -> [Matrix a]
collapse m = cp (map cp m)

solve :: Grid -> [Grid]
solve = filter valid . collapse . choices

prune :: Matrix Choices -> Matrix Choices
prune = pruneBy boxs . pruneBy cols . pruneBy rows

prune' :: Matrix Choices -> Matrix Choices
prune' = pruneBy' boxs . pruneBy' cols . pruneBy' rows

-- fix rs = until cond reduse rs
-- where cond rs = rs == re

pruneBy' f = f . map reduse . f

pruneBy f = f . map reduce . f

-- reduce :: row choices -> row choices
reduce rs = until cond reduse rs
  where
    cond rs = rs == reduse rs

-- reduse :: row choices -> row choices
reduse xss = foldr helper [] xss
  where
    singles = [head xs | xs <- xss, single xs]
    helper xs acc
      | single xs = xs : acc
      | otherwise = (foldr (\y acc -> delete y acc) xs singles) : acc

-- delelte for last occurance of element
delete :: (Eq a) => a -> [a] -> [a]
delete key xs = snd $ foldr helper (False, []) xs
  where
    helper x (True, acc) = (True, x : acc)
    helper x (False, acc)
      | key == x = (True, acc)
      | otherwise = (False, x : acc)

-- cond :: [[a]] -> bool
-- cond = all single
-- cond xs =

single [_] = True
single _ = False

solve1 :: Grid -> [Grid]
solve1 = filter valid . collapse . prune . choices

fix :: (Eq a) => (a -> a) -> a -> a
fix f xs = until cond f xs
  where
    cond xs = xs == f xs

solve2 :: Grid -> [Grid]
solve2 = filter valid . collapse . fix prune' . choices

easy :: Grid
easy =
  [ "2....1.38",
    "........5",
    ".7...6...",
    ".......13",
    ".981..257",
    "31....8..",
    "9..8...2.",
    ".5..69784",
    "4..25...."
  ]

consistent :: Row Choices -> Bool
consistent xss = not $ hasDup [head xs | xs <- xss, single xs]
