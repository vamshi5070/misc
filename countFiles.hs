import System.Directory
import System.FilePath
import Control.Monad

countFilesWithExtension :: FilePath -> String -> IO Int
countFilesWithExtension dir ext = do
  isDir <- doesDirectoryExist dir
  if not isDir
    then return 0
    else do
      contents <- listDirectory dir
      let filePaths = map (dir </>) contents
      files <- filterM doesFileExist filePaths
      let matchingFiles = filter ((== ext) . takeExtension) files
      subdirs <- filterM doesDirectoryExist filePaths
      counts <- mapM (`countFilesWithExtension` ext) subdirs
      return $ length matchingFiles + sum counts

main :: IO ()
main = do
  let dir = "../yi"
      ext = ".hs"
  count <- countFilesWithExtension dir ext
  putStrLn $ "There are " ++ show count ++ " files with extension " ++ ext ++ " in " ++ dir
