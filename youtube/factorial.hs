fac 0 = 1
fac n = n * fac (n-1)

main = do
  putStr "Factorial of 5 is, "
  print (fac 5)