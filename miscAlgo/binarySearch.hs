binarySearch' :: (Ord a) => [a] -> a -> Maybe Int
binarySearch' xs x = go 0 (length xs - 1)
  where
    go low high
      | low > high = Nothing
      | x == xs !! mid = Just mid
      | x < xs !! mid = go low (mid - 1)
      | x > xs !! mid = go (mid + 1) high
      where
        mid = low + (high - low) `div` 2


-- binarySearch ::
-- binarySearch [] _ = Nothing
-- binarySearch xs key 
  -- | mid == key = Just key
    -- where mid = xs !! (length xs `div` 2)


linearSearch :: (Eq a) =>  [a] -> a -> Maybe Int
-- linearSearch [] _ = Nothing
linearSearch (xs) key = (\(cnt,p) -> if p then (Just cnt) else Nothing) $  foldr helper (length xs, False) xs
  where helper x (cnt,True) = (cnt,True)
        helper x (cnt,False)
          | x == key = (cnt-1,True)
          | otherwise = (cnt-1,False)
       
            -- | x == key = Just

binarySearch ::  (Ord a) => [a] -> a -> Maybe Int
binarySearch xs key = go (length xs - 1) 0 
  where go high low 
          | (low > high)   = Nothing
          | (xs !! mid)  == key = Just mid
          | (xs !! mid) > key = go (mid-1) low 
          | (xs !! mid) < key = go high (mid+1) 
            where mid = low + (high - low)  `div` 2
                        -- case low > high of
            -- False -> 
            -- if low > high
         -- then Nothing
         -- else  xs !! mid == key = Just mid



