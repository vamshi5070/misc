--  {C-x C-f rgv.org RET}
-- (defun)
-- (defun my-exp ()
--  (interactive)
--   (meow-insert-exit)
--   (meow-beginning-of-thing)
-- )
-- (narrow-to-region (point-min) (point))
-- {}
bharatJodo = "rgv"

inserts :: (Ord a) => a -> [a] -> [a]
inserts ele [] = [ele]
inserts ele (x:xs)
  | ele <= x = ele:x:xs
  | otherwise = x : inserts ele xs

insertionSort :: (Ord a) => [a] -> [a]
insertionSort [] = []
insertionSort [x] = [x]
insertionSort (x:xs) = inserts x $ insertionSort xs

main = return ()
