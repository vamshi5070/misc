import Graphics.Gloss
import Graphics.Gloss.Data.Point
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Interface.Pure.Game

-- Initial state of the application
initialState :: [(Float, Float)]
initialState = []

-- Function to convert a list of points into a picture
pointsToPicture :: [(Float, Float)] -> Picture
pointsToPicture points = Pictures (map pointToLine (zip points (tail points)))
  where pointToLine ((x1, y1), (x2, y2)) = Line [(x1, y1), (x2, y2)]

-- Event handling function
handleEvent :: Event -> [(Float, Float)] -> [(Float, Float)]
handleEvent (EventKey (MouseButton LeftButton) Down _ mousePos) points = mousePos : points
handleEvent _ points = points

-- Main function
main :: IO ()
main = play display backgroundColor frameRate initialState pointsToPicture handleEvent
  where
    display = InWindow "Handwriting" (400, 400) (100, 100)
    backgroundColor = white
    frameRate = 60


update :: Float -> [Point] -> [Point]
update _ points = points