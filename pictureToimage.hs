import Codec.Picture
import Codec.Picture.Types
import Graphics.Gloss

-- A sample Picture
picture :: Picture
picture = pictures [circle 100, rectangleWire 200 200]

-- Render the Picture into a Bitmap
renderPic :: Picture -> Image PixelRGBA8
renderPic pic =
   let (w,h) = (800, 600)
       img = generateImage (\x y -> let c = pixelAt pic x y
                                   in PixelRGBA8 c c c 255) w h
   in img

main :: IO ()
main = do
  -- Convert the Bitmap into an Image PixelRGBA8
  let img = renderPic picture
  -- Save the Image as a PNG file
  savePngImage "sample.png" img
